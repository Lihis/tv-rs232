/* tv.h
 *
 * Copyright 2019-2021 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TV_RS232_TV_H
#define TV_RS232_TV_H

struct SerialPort {
    char *port;
    int baudrate;
    char *parity;
};

enum Command {
    Unknown = 0,
    Remote = 1,
    Power = 2,
    Volume = 3,
    Input = 4
};

enum Remote {
    Enable = 0,
    Disable = 1,
    Toggle = 2
};

enum Power {
    On = 0,
    Off = 1,
    Status = 2
};

enum Volume {
    Up = 0,
    Down = 1
};

enum Input {
    CableTV = 0,
    Analogue = 1,
    AV = 2,
    Component = 4,
    RGB = 6,
    HDMI = 7,
    Query = 255,
};

enum InputSource {
    Source1 = 0,
    Source2 = 1,
    Source3 = 2,
    Source4 = 3
};

int tv_init(struct SerialPort settings);
void tv_close(int port);

int send_command(int port, enum Command command, int action, int param);

#endif //TV_RS232_TV_H
