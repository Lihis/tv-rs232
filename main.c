/* main.c
 *
 * Copyright 2019-2021 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <string.h>
#include "tv.h"
#include "version.h"

void print_help() {
    printf("Usage: tv-rs232 [OPTION]\n");
    printf("\n");
    printf("     --remote-enable        Enable remote control\n");
    printf("     --remote-disable       Disable remote control\n");
    printf("     --remote-toggle        Toggle remote control enable/disable\n");
    printf("     --power-on             Turn TV on\n");
    printf("     --power-off            Turn TV off\n");
    printf("     --power-status         Query TV on/off status\n");
    printf("     --volume-up            Increase volume\n");
    printf("     --volume-down          Decrease volume\n");
    printf("     --input                Get currently selected input\n");
    printf("     --input-HDMI1          Set input to HDMI 1\n");
    printf("     --input-HDMI2          Set input to HDMI 2\n");
    printf("     --version              Print program version\n");
    printf("     --help                 Print this help and exit\n");
}

int main(int argc, char *argv[]) {
    enum Command command = Unknown;
    int action = 0;
    int param = 0;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--remote-enable") == 0) {
            command = Remote;
            action = Enable;
        } else if (strcmp(argv[i], "--remote-disable") == 0) {
            command = Remote;
            action = Disable;
        } else if (strcmp(argv[i], "--remote-toggle") == 0 ) {
            command = Remote;
            action = Toggle;
        } else if (strcmp(argv[i], "--power-on") == 0) {
            command = Power;
            action = On;
        } else if (strcmp(argv[i], "--power-off") == 0) {
            command = Power;
            action = Off;
        } else if (strcmp(argv[i], "--power-status") == 0) {
            command = Power;
            action = Status;
        } else if (strcmp(argv[i], "--volume-up") == 0) {
            command = Volume;
            action = Up;
        } else if (strcmp(argv[i], "--volume-down") == 0) {
            command = Volume;
            action = Down;
        } else if (strcmp(argv[i], "--input") == 0) {
            command = Input;
            action = Query;
        } else if (strcmp(argv[i], "--input-HDMI1") == 0) {
            command = Input;
            action = HDMI;
            param = Source1;
        } else if (strcmp(argv[i], "--input-HDMI2") == 0) {
            command = Input;
            action = HDMI;
            param = Source2;
        } else if (strcmp(argv[i], "--version") == 0) {
            printf("tv-rs232 v" TVRS232_VERSION "\n");
            return 0;
        } else if (strcmp(argv[i], "--help") == 0) {
            print_help();
            return 0;
        } else {
            fprintf(stderr, "Unknown option '%s'\n", argv[i]);
            return 1;
        }
    }

    if (command == Unknown) {
        print_help();
        return 1;
    }

    struct SerialPort settings;
    settings.port = "/dev/tv_rs232";
    settings.baudrate = 9600;
    settings.parity = "NO";
    if (tv_init(settings) != 0) {
        return -1;
    }

    int ret = send_command(0, command, action, param);
    if (ret != 0) {
        fprintf(stderr, "Failed to execute command\n");
    }
    tv_close(0);

    return ret;
}
