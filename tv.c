/* tv.c
 *
 * Copyright 2019-2021 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tv.h"
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <poll.h>
#include <stdlib.h>

struct pollfd ports[1];

int open_serial_port(const char *serial_port) {
    fprintf(stderr, "Using port: %s\n", serial_port);
    return open(serial_port, O_RDWR | O_NOCTTY);
}

int configure_port(const int port, const int baudrate, const char *parity) {
    int errno;
    errno = 0;

    struct termios tty;
    memset(&tty, 0, sizeof tty);

    if (tcgetattr(port, &tty) != 0) {
        fprintf(stderr, "Unable to read serial port current parameters\n");
        return errno;
    }

    /* Set Baud Rate */
    cfsetospeed(&tty, (speed_t)baudrate);
    cfsetispeed(&tty, (speed_t)baudrate);

    /* Setting other Port Stuff */
    if (strcmp(parity, "NO") == 0) {
        // NO PARITY
        tty.c_cflag &= ~PARENB;        // Make 8n1
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS8;
    } else if (strcmp(parity, "EVEN") == 0) {
        tty.c_cflag |= PARENB;
        tty.c_cflag &= ~PARODD;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS7;
    } else if (strcmp(parity, "ODD") == 0) {
        tty.c_cflag |= PARENB;
        tty.c_cflag |= PARODD;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS7;
    } else if (strcmp(parity, "MARK") == 0) {
        tty.c_cflag &= ~PARENB;
        tty.c_cflag |= CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS7;
    } else if (strcmp(parity, "SPACE") == 0) {
        tty.c_cflag &= ~PARENB;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS8;
    } else {
        fprintf(stderr, "Unkown type for parity\n");
        return -1;
    }

    tty.c_cflag &= ~CRTSCTS;        // no flow control
    tty.c_cc[VMIN] = 1;             // read doesn't block
    tty.c_cc[VTIME] = 7;            // 0.5 second read timeout
    tty.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines

    /* Make raw */
    cfmakeraw(&tty);

    /* Flush Port and apply port parameters */
    tcflush(port, TCIFLUSH);
    if (tcsetattr(port, TCSANOW, &tty) != 0) {
        fprintf(stderr, "Unable to set serial port parameters\n");
    }

    return 0;
}

int tv_init(struct SerialPort settings) {
    ports[0].fd = open_serial_port(settings.port);
    if (ports[0].fd < 0) {
        return -1;
    }

    ports[0].events = POLLIN | POLLHUP;

    return configure_port(ports[0].fd, settings.baudrate, settings.parity);
}

void tv_close(int port) {
    if (ports[port].fd >= 0) {
        close(ports[port].fd);
    }
}

char *send_serial(int port, char *command) {
    int written;
    static char buffer[10] = {'\0'};
    ssize_t ret = 0;
    int timeouts = 0;

    written = write(port, command, strlen(command));
    if(written == -1) {
        fprintf(stderr, "Error while sending command to TV\n");
        return NULL;
    }

    while (ret < 10) {
        int poll_ret = poll(ports, 1, 1000);

        if (poll_ret < 0) {
            break;
        } else if (poll_ret > 0) {
            if (ports[0].revents & POLLIN) {
                ret += read(port, (buffer + ret), 10);
            } else {
                fprintf(stderr, "Unhandled poll\n");
                timeouts++;
            }
        } else {
            timeouts++;
        }

        if (timeouts > 5) {
            break;
        }
    }

    return buffer;
}

int remote(int port, enum Remote action) {
    char *response;

    if (action == Enable || action == Disable) {
        response = send_serial(port, (action == Enable ? "km 00 00\r" : "km 00 01\r"));
        if (response == NULL) {
            return -1;
        } else if (strlen(response) < 10) {
            fprintf(stderr, "Remote: too short response\n");
            return -1;
        } else {
            return 0;
        }
    }

    response = send_serial(port, "km 00 FF\r");
    if (response == NULL) {
        return -1;
    } else if (strlen(response) < 10) {
        fprintf(stderr, "Remote: too short response\n");
        return -1;
    } else {
        fprintf(stdout, "Received: %s\n", response);
    }

    if (response[5] != 'O' && response[6] != 'K') {
        fprintf(stderr, "Remote: acknowledge error\n");
        return -1;
    }

    return remote(port, (response[8] == '0' ? Disable : Enable));
}

int power(int port, enum Power action) {
    char cmd[10];
    char *val;

    switch (action) {
        case On:
            val = "01";
            break;
        case Off:
            val = "00";
            break;
        case Status:
        default:
            val = "FF";
            break;
    }
    snprintf(cmd, 10, "ka 00 %s\r", val);

    char *response = send_serial(port, cmd);
    if (response == NULL) {
        return -1;
    } else if (strlen(response) < 10) {
        fprintf(stderr, "Power: too short response\n");
        return -1;
    } else {
        if (action == Status) {
            printf("%c\n", response[8]);
        }
        return 0;
    }
}

int volume(int port, enum Volume action) {
    char *response;
    int volume;

    response = send_serial(port, "kf 00 FF\r");
    if (response == NULL) {
        return -1;
    } else if (strlen(response) < 10) {
        fprintf(stderr, "Volume: too short response\n");
        return -1;
    } else {
        fprintf(stdout, "Received: %s\n", response);
    }

    if (response[5] != 'O' && response[6] != 'K') {
        fprintf(stderr, "Volume: acknowledge error\n");
        return -1;
    }

    volume = strtol(response + 7, NULL, 16);

    switch (action) {
        case Down:
            if ((volume - 1) >= 0) {
                volume--;
            }
            break;
        case Up:
            if ((volume + 1) <= 100) {
                volume++;
            }
            break;
        default:
            break;
    }

    char cmd[10] = { '\0' };
    sprintf(cmd, "kf 00 %02x\r", volume);
    send_serial(port, cmd);

    return 0;
}

int input(int port, enum Input action, enum InputSource source) {
    char cmd[10];
    char *response;

    snprintf(cmd, 10, "xb 00 %02X\r", (action == Query ? 0xFF : (action << 4) + (source)));
    response = send_serial(port, cmd);
    if (action == Query) {
        char cur[3];
        cur[0] = response[7];
        cur[1] = response[8];
        cur[2] = '\0';

        int val = (int)strtol(cur, NULL, 16);
        char *cur_input;
        int cur_source;

        fprintf(stderr, "Response: %s\n", response);
        fprintf(stderr, "From response: %s (%d)\n", cur, val);

        switch ((enum Input)val >> 4) {
            case CableTV:
                cur_input = "CableTV";
                break;
            case Analogue:
                cur_input = "Analogue";
                break;
            case AV:
                cur_input = "AV";
                break;
            case Component:
                cur_input = "Component";
                break;
            case RGB:
                cur_input = "RGB";
                break;
            case HDMI:
            default:
                cur_input = "HDMI";
                break;
        }

        cur_source = val & 0x0F;

        fprintf(stdout, "%s %d\n", cur_input, (cur_source + 1));
    }

    return 0;
}

int send_command(int port, enum Command command, int action, int param) {
    int ret = -1;

    switch (command) {
        case Remote:
            ret = remote(ports[port].fd, action);
            break;
        case Power:
            ret = power(ports[port].fd, action);
            break;
        case Volume:
            ret = volume(ports[port].fd, action);
            break;
        case Input:
            ret = input(ports[port].fd, action, param);
            break;
        default:
            break;
    }

    return ret;
}
