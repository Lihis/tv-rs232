cmake_minimum_required(VERSION 3.7.2)
project(tv-rs232
        VERSION 0.1.0
        LANGUAGES C
        )

set(CMAKE_C_STANDARD 99)

configure_file(version.h.in version.h)

add_executable(tv-rs232 main.c tv.c tv.h)

target_include_directories(${PROJECT_NAME}
                           PRIVATE
                           ${CMAKE_CURRENT_BINARY_DIR}
                           )

install(TARGETS tv-rs232
        RUNTIME DESTINATION bin
        )
